module "azurerm_mysql" {
    source = "git::https://gitlab.com/azure-iac2/root-modules.git//mysql" 
    business_divsion            = var.business_divsion
    environment                 = var.environment
    resource_group_name         = data.azurerm_resource_group.rg.name
    location                    = data.azurerm_resource_group.rg.location
    my_server_name              = var.my_server_name
    my_server_admin_login       = var.my_server_admin_login
    my_server_admin_password    = var.my_server_admin_password

    my_server_firewall_rule_name       = var.my_server_firewall_rule_name
    my_db_name                         = var.my_db_name
}